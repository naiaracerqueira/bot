import pandas as pd
import numpy as np
import re
import time
from sheets import leitor_sheet, escritor_sheet
from datetime import date

documento = 'Unimed - Campanhas'
# 9 colunas:
col_name = ['Data','Canal','Objetivo','Grupo de Campanha','Campanha','Impressões','Cliques','Conversões',
            'Visualização de Vídeo','Investimento']

def cria_dataset():
    facebook = leitor_sheet(documento, 'Facebook Ads')
    linkedin = leitor_sheet(documento, 'Linkedin Ads')
    google = leitor_sheet(documento, 'Google Ads')

    #Fazendo ajustes nas tabelas para que elas sejam concatenáveis
    facebook = facebook.rename(columns={'Date':'Data',
                                        'Campaign objective':'Objetivo',
                                        'Campaign name':'Campanha',
                                        'Impressions':'Impressões',
                                        'Link clicks':'Cliques',
                                        'Cost':'Investimento',
                                        'ThruPlay actions':'Visualização de Vídeo'})
    facebook['Canal'] = 'Facebook'
    facebook['Grupo de Campanha'] = '-'
    facebook['Conversões'] = facebook['On-Facebook leads'] + facebook['Website leads']

    linkedin = linkedin.rename(columns={'Date':'Data',
                                        'Account name':'Conta',
                                        'Campaign objective type':'Objetivo',
                                        'Campaign group name':'Grupo de Campanha',
                                        'Campaign name':'Campanha',
                                        'Impressions':'Impressões',
                                        'Clicks':'Cliques',
                                        'Total spent':'Investimento',
                                        'Conversions':'Conversões',
                                        'Video views ':'Visualização de Vídeo'})
    linkedin['Canal'] = 'Linkedin'

    google = google.rename(columns={'Date':'Data',
                                    'Campaign name':'Campanha',
                                    'Impressions':'Impressões',
                                    'Clicks':'Cliques',
                                    'Cost':'Investimento',
                                    'Conversions':'Conversões',
                                    'Video views':'Visualização de Vídeo'})
    google['Canal'] = 'Google ' + google['Advertising channel type']
    google['Grupo de Campanha'] = '-'
    google['Objetivo'] = '-'

    # Une os três
    dataset = pd.concat([facebook,linkedin,google])
    dataset = dataset[col_name]
    dataset["Visualização de Vídeo"].fillna(0, inplace=True)
    dataset = dataset.astype(str)
    print("Dataset criado")
    dataset.sort_values(by='Data', ascending=True, inplace=True)
    #dataset.to_csv('dataset.csv')
    return dataset


def confere_data_sheets():
    df = leitor_sheet(documento, '[BOT] Geral')
    data = df.Data.max()
    return data


def escreve_lista(dataset):
    data_sheets = confere_data_sheets()
    dataset = dataset[dataset.Data > data_sheets]

    #Envia pro Sheets linha a linha
    for row_index, row in dataset.iterrows():
        escritor_sheet(documento, '[BOT] Geral', list(row))
        time.sleep(5)


if __name__ == "__main__":
    dataset = cria_dataset()
    escreve_lista(dataset)