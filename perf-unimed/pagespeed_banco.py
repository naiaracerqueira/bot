import requests
import os
import pandas as pd
import time
from datetime import datetime
from sheets import leitor_sheet
# gera classe que sqlalchemy usa, que gerencia as coisas:
from sqlalchemy.ext.declarative import declarative_base
# sqlalchemy: orm x core 
from sqlalchemy.orm import sessionmaker
from sqlalchemy import *
# interface de mysql para python:
import pymysql
# Documentation: https://developers.google.com/speed/docs/insights/v5/get-started
# JSON paths: https://developers.google.com/speed/docs/insights/v4/reference/pagespeedapi/runpagespeed
# Populate 'pagespeed.txt' file with URLs to query against API.


#### url para conectar ao banco da komuh: ####
K_HOST = "34.193.166.110"
K_USUARIO = "datastudio"
K_PASSWORD = "h4a5ti%GR#EvDbn$3"
K_BASE = "itatiaia_komuh"

banco_komuh = "mysql+pymysql://"+K_USUARIO+":"+K_PASSWORD+"@"+K_HOST+"/"+K_BASE
engine_komuh = create_engine(banco_komuh)
Session_komuh = sessionmaker(bind=engine_komuh)
session_komuh = Session_komuh()

Base = declarative_base()
metadata = MetaData()


class Pagespeed(Base):
    __tablename__ = "pagespeed"
    table_id = Column('table_id', Integer, primary_key=True, autoincrement=True)
    data_insert = Column('data_insert', DateTime)
    url = Column('url', String(200))
    fcp = Column('fcp', String(6))
    tti = Column('tti', String(6))

    def __repr__(self):
        return self.table_id


    def cria_tabela(self):
        Base.metadata.create_all(engine_komuh, checkfirst=True)


    def lista_urls(self):
        arquivo = open('/home/ubuntu/bot/perf-unimed/recursos/urls.txt', "r", encoding="utf-8")
        arquivo = arquivo.readlines()
        lista = [ line.rstrip('\n') for line in arquivo ]
        return lista


    def requisicao(self, url_api):
        r = requests.get(url_api)
        saida = r.json()
        return saida


    def transformador(self, entrada):
        urlfcp = entrada['lighthouseResult']['audits']['first-contentful-paint']['displayValue']
        fcp = urlfcp.replace('\xa0s', '')
        urltti = entrada['lighthouseResult']['audits']['interactive']['displayValue']
        tti = urltti.replace('\xa0s', '')
        lista = [fcp, tti]
        return lista


    def postador(self):
        page = Pagespeed()
        urls = page.lista_urls()
        
        for url in urls:
            url_api = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url='+url+'&strategy=mobile'
            print('Requesting '+url+'...')

            final = page.requisicao(url_api)
            try:
                lista = page.transformador(final)
                print(lista)

                entrada = Pagespeed(url = url,
                                    fcp = lista[0],
                                    tti = lista[1],
                                    data_insert = datetime.now()
                )
                session_komuh.add(entrada)
                session_komuh.commit()

            except:
                pass
            
            time.sleep(60)

    # def antigos(self):
    #     df = leitor_sheet('Itatiaia - PageSpeed', 'results')

    #     for i in range(df.shape[0]):
    #         entrada = Pagespeed(data_insert = df.loc[i,'Data'],
    #                             url = df.loc[i,'URL'],
    #                             fcp = str(df.loc[i,'First Contentful Paint (s)']),
    #                             tti = str(df.loc[i,'Time to Interactive (s)'])
    #         )
    #         session_komuh.add(entrada)
    #         session_komuh.commit()


if __name__ == "__main__":
    page = Pagespeed()
    # page.cria_tabela()
    page.postador()
    # page.antigos()


