import pandas as pd
import re
import time
from sheets import leitor_sheet, escritor_sheet
from datetime import date

def dataset_ambev():
    documento = 'Ambev - Campanhas'
    facebook = leitor_sheet(documento, 'Facebook Ads')
    
    df = leitor_sheet(documento, '[BOT] Geral')
    data = df.Date.max()

    facebook = facebook[facebook.Date > data]

    for row_index, row in facebook.iterrows():
        escritor_sheet(documento, '[BOT] Geral', list(row))
        time.sleep(5)

def dataset_mikes():
    documento = 'Mikes - Campanhas'
    facebook = leitor_sheet(documento, 'Facebook Ads')
    
    df = leitor_sheet(documento, '[BOT] FaceAds')
    data = df.Date.max()

    facebook = facebook[facebook.Date > data]

    for row_index, row in facebook.iterrows():
        escritor_sheet(documento, '[BOT] FaceAds', list(row))
        time.sleep(5)

if __name__ == "__main__":
    dataset_mikes()
    dataset_ambev()