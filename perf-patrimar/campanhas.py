import pandas as pd
import numpy as np
import re
import time
from sheets import leitor_sheet, escritor_sheet
from datetime import date

documento = 'Patrimar - Campanhas'
# 9 colunas:
col_name = ['Data','Conta','Canal','Objetivo', 'Grupo de Campanha','Campanha','Impressões','Cliques','Leads','Investimento']

def cria_dataset():
    facebook = leitor_sheet(documento, 'Facebook Ads')
    linkedin = leitor_sheet(documento, 'Linkedin Ads')
    google = leitor_sheet(documento, 'Google Ads')

    #Fazendo ajustes nas tabelas para que elas sejam concatenáveis
    facebook = facebook.rename(columns={'Date':'Data',
                                        'Account':'Conta',
                                        'Campaign objective':'Objetivo',
                                        'Campaign name':'Campanha',
                                        'Impressions':'Impressões',
                                        'Link clicks':'Cliques',
                                        'Cost':'Investimento'})
    facebook['Canal'] = 'Facebook'
    facebook['Grupo de Campanha'] = '-'
    facebook['Conta'] = facebook['Conta'].map({'Construtora Novolar':'Novolar','Construtora Patrimar':'Patrimar'})
    facebook['Leads'] = facebook['Website leads'] + facebook['On-Facebook leads']

    linkedin = linkedin.rename(columns={'Date':'Data',
                                        'Account name':'Conta',
                                        'Campaign objective type':'Objetivo',
                                        'Campaign group name':'Grupo de Campanha',
                                        'Campaign name':'Campanha',
                                        'Impressions':'Impressões',
                                        'Clicks':'Cliques',
                                        'Total spent':'Investimento'})
    linkedin['Canal'] = 'Linkedin'

    google = google.rename(columns={'Date':'Data',
                                    'Account':'Conta',
                                    'Campaign name':'Campanha',
                                    'Impressions':'Impressões',
                                    'Clicks':'Cliques',
                                    'Conversions':'Leads',
                                    'Cost':'Investimento'})
    google['Canal'] = 'Google ' + google['Advertising channel type']
    google['Grupo de Campanha'] = '-'
    google['Objetivo'] = '-'
    google['Conta'] = google['Conta'].map({'NOVOLAR':'Novolar','Patrimar - Liberty (Cyrela)':'Patrimar'})

    # Une os três
    dataset = pd.concat([facebook,linkedin,google])
    dataset = dataset[col_name]

    dataset = dataset.astype(str)
    print("Dataset criado")
    dataset.sort_values(by='Data', ascending=True, inplace=True)
    #dataset.to_csv('dataset.csv')
    return dataset


def confere_data_sheets():
    df = leitor_sheet(documento, '[BOT] Geral')
    data = df.Data.max()
    return data


def escreve_lista(dataset):
    data_sheets = confere_data_sheets()
    hoje = date.today().strftime("%Y-%m-%d")

    dataset = dataset[dataset.Data > data_sheets]
    dataset = dataset[dataset.Data != hoje]

    #Envia pro Sheets linha a linha
    for row_index, row in dataset.iterrows():
        escritor_sheet(documento, '[BOT] Geral', list(row))
        time.sleep(5)


if __name__ == "__main__":
    dataset = cria_dataset()
    escreve_lista(dataset)