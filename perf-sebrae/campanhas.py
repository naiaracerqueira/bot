import pandas as pd
import numpy as np
import gspread
import re
import time
from sheets import leitor_sheet, escritor_sheet, deleta_linhas
from datetime import date

documento = 'Sebrae Mateus - Estudos'

# 14 colunas
col_name = ['Date','Canal','Campaign group name','Campaign name','Reach','Impressions','CPM','Clicks','CPC','CTR',
            'Video views','Video completions','Total engagements','ThruPlay actions','Cost per ThruPlay',
            'Total spent']

def cria_dataset():
    facebook = leitor_sheet(documento, 'Facebook')
    linkedin = leitor_sheet(documento, 'LinkedIn')
    google = leitor_sheet(documento, 'Google')

    #Fazendo ajustes nas tabelas para que elas sejam concatenáveis
    # Campaign name | Impressions | CPM (cost per 1000 impressions) | Clicks (all) | CPC (all) | CTR (all) |
    # ThruPlay actions | Cost per ThruPlay | Cost
    facebook = facebook.rename(columns={'CPM (cost per 1000 impressions)':'CPM','Link clicks':'Clicks','CPC (cost per link click)':'CPC','CTR (link click-through rate)':'CTR','Cost':'Total spent'})
    facebook['Campaign group name'] = '-'
    facebook[['Reach','Video views','Video completions','Total engagements']] = 0
    facebook['Canal'] = 'Facebook'

    # Campaign group name | Campaign name | Reach | Impressions | CPM | Clicks | CPC | CTR | Video views |
    # Video completions | Total engagements | Total spent
    linkedin['ThruPlay actions','Cost per ThruPlay'] = 0
    linkedin['Canal'] = 'Linkedin'

    # Campaign name | Impressions | CPM | Clicks | CPC | CTR | Video views | Cost
    google = google.rename(columns={'Cost':'Total spent'})
    google['Campaign group name'] = '-'
    google[['Reach','Video completions','Total engagements','ThruPlay actions','Cost per ThruPlay']] = 0
    google['Canal'] = 'Google'

    # Une os três
    dataset = pd.concat([facebook,linkedin,google])
    dataset = dataset[col_name]

    col_num = dataset.select_dtypes('number').columns
    dataset[col_num] = dataset.select_dtypes('number').fillna(0)

    col_obj = dataset.select_dtypes('object').columns
    dataset[col_obj] = dataset.select_dtypes('object').fillna('-')

    dataset = dataset.astype(str)
    print("Dataset criado")
    return dataset


def confere_data_sheets():
    df = leitor_sheet(documento, 'Geral (BOT)')
    data = df.Date.max()
    return data


def escreve_lista(dataset):
    data_sheets = confere_data_sheets()
    hoje = date.today().strftime("%Y-%m-%d")

    dataset = dataset[dataset.Date > data_sheets]
    dataset = dataset[dataset.Date != hoje]

    #Limpa a sheet
    # deleta_linhas(documento, 'Geral (BOT)', 2, 100000)

    #Envia pro Sheets linha a linha
    for row_index, row in dataset.iterrows():
        escritor_sheet(documento, 'Geral (BOT)', list(row))
        time.sleep(5)


if __name__ == "__main__":
    df = cria_dataset()
    escreve_lista(df)