import pandas as pd
import numpy as np
import gspread
import re
import time
from sheets import leitor_sheet, escritor_sheet, deleta_linhas
from datetime import date

documento = '1898.3 - HP - Overview Campanhas | FEVEREIRO 3ª Semana 2021'

# 14 colunas
col_name = ['Date','Canal','Campaign name','Cost','Impressions','Clicks','Conversions']

def cria_dataset():
    facebook = leitor_sheet(documento, 'Facebook Ads')
    google = leitor_sheet(documento, 'Google')

    #Fazendo ajustes nas tabelas para que elas sejam concatenáveis
    facebook = facebook.rename(columns={'Link clicks':'Clicks'})
    facebook['Conversions'] = facebook['Website adds to cart'] + facebook['Website leads']
    facebook['Canal'] = 'Facebook'
    facebook = facebook[col_name]

    google['Canal'] = 'Google'
    google = google[col_name]

    # Une os dois
    dataset = pd.concat([facebook,google])

    dataset = dataset.astype(str)
    dataset = dataset.sort_values(by='Date', ascending=True)
    print("Dataset criado")

    #dataset.to_csv("~/perf-pardini/dataset.csv")
    #print(dataset.head())
    return dataset


def confere_data():
    df = leitor_sheet(documento, '[BOT] Geral')
    data = df.Date.max()
    return data


def escreve_lista(df):
    data_max = confere_data()
    hoje = date.today().strftime("%Y-%m-%d")

    df = df[df.Date > data_max]
    df = df[df.Date != hoje]

    #Envia pro Sheets linha a linha
    for row_index, row in df.iterrows():
        escritor_sheet(documento, '[BOT] Geral', list(row))
        time.sleep(5)


if __name__ == "__main__":
    df = cria_dataset()
    escreve_lista(df)